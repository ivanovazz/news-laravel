<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news',function (Blueprint $table){
            $table->longText('small_text')->nullable()->change();
            $table->longText('text')->nullable()->change();
            $table->boolean('published')->default(0)->change();
            $table->dateTimeTz('published_date_time')->nullable()->change();
            $table->string('img')->nullable()->change();
            $table->string('title_seo')->nullable()->change();
            $table->string('keywords_seo')->nullable()->change();
            $table->string('description_seo')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
