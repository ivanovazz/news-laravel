<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('small_text');
            $table->longText('text');
            $table->integer('author_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->boolean('published');
            $table->dateTimeTz('published_date_time');
            $table->string('img');
            $table->string('slug_url');
            $table->string('title_seo');
            $table->string('keywords_seo');
            $table->string('description_seo');
            $table->timestamps();
            $table->foreign('author_id')->references('id')->on('authors');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
