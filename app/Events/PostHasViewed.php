<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Article;

class PostHasViewed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $article;
    public $ip;
    public $article_id;
    public $date;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Article $article, $ip, $article_id, $date)
    {
        $this->article = $article;
        $this->ip = $ip;
        $this->article_id = $article_id;
        $this->date = $date;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
