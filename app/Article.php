<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use Illuminate\Support\Facades\DB;
//use Laravel\Scout\Searchable;

class Article extends Model
{
    //use Searchable;
    protected $table='articles';
    protected $fillable=['title','small_text','text','author_id','category_id', 'published', 'published_date_time', 'img', 'slug_url', 'title_seo', 'keywords_seo', 'description_seo'];
    public function fileUpload(Request $request){
        if(!$this->id){
            throw new Exception('no id');
        }
        if($request->isMethod('post')){

            if($request->hasFile('image')) {
                $file = $request->file('image');
                $type=$file->getMimeType();
                $imgType=explode('/',$type);
                $fileName=$this->id.'-img_'.time().'.'.$imgType[1];
                $file->move(public_path() . '/img',$fileName);
                $this->img=$fileName;
                $this->save();
            }
        }
    }
    public static function categoryArticles($id){
        $categoryArticles=Article::where('category_id','=',$id)->where('published','=',1)->orderBy('published_date_time','desc')->skip(0)->take(5)->get();
        return $categoryArticles;
    }
    public static function popularArticles(){
        $popularArticles=Article::where('published','=',1)->orderBy('view_count','desc')->skip(0)->take(5)->get();
        return $popularArticles;
    }
}
