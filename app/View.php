<?php

namespace App;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $table='views';
    protected $fillable=['ip','date','article_id'];
    public static function add(string $ip, $article_id, $date){
        $view = new View();
        $view->date = $date;
        $view->ip = $ip;
        $view->article_id = $article_id;
        $view->save();
    }
}
