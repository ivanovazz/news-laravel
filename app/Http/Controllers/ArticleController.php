<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 22.07.17
 * Time: 17:31
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Article;
use App\Category;
use App\Author;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class ArticleController extends Controller
{
    public function data(){
        //$admin_name=Auth::user()->login;
        $articles=\App\Article::all();
        return view('admin.article', ['articles'=>$articles]);
    }
    public function beforeAdd(){
        //$admin_name=Auth::user()->login;
        $categories = \App\Category::all();
        $authors = \App\Author::all();
        return view('admin.addArticle', ['categories'=>$categories, 'authors'=>$authors]);
    }
    public function add(Request $request){
        $article = new Article();
        $article->title=$request->title;
        $article->small_text=$request->small_text;
        $article->text=$request->text;
        $article->author_id=$request->author_id;
        $article->category_id=$request->category_id;
        $article->published=$request->published;
        $article->published_date_time=$request->published_date_time;
        $article->slug_url=$request->slug_url;
        $article->title_seo=$request->title_seo;
        $article->keywords_seo=$request->keywords_seo;
        $article->description_seo=$request->description_seo;
        $article->save();

        $article->fileUpload($request);

        /*if($request->isMethod('post')){

            if($request->hasFile('image')) {
                $file = $request->file('image');
                $file->move('img','filename.jpg');
            }
        }*/
        return redirect(route('admin_articles'));
    }
    public function delete($id){
        if($id){
            $delete=\App\Article::find($id);
            File::delete(public_path().'/img/'.$delete->img);
            $delete->delete();
        }
        return redirect(route('admin_articles'));
    }
    public function beforeUpdate($id){
        if($id){
            //$admin_name=Auth::user()->login;
            $categories = \App\Category::all();
            $authors = \App\Author::all();
            $articleBeforeUpdate=\App\Article::find($id);
            return view('admin.updateArticle',['categories'=>$categories, 'authors'=>$authors, 'articleBeforeUpdate'=>$articleBeforeUpdate]);
        }
    }
    public function update(Request $request, $id){
        if($request->route('id')){
            $update=\App\Article::find($id);

          //  dd($update);

            $lastImg=$update->img;
            $update->title=$request->title;
            $update->small_text=$request->small_text;
            $update->text=$request->text;
            $update->author_id=$request->author_id;
            $update->category_id=$request->category_id;
            if($request->published==null){
                $update->published=0;
            }else{
                $update->published=$request->published;
            }

            $update->published_date_time=$request->published_date_time;
            $update->slug_url=$request->slug_url;
            $update->title_seo=$request->title_seo;
            $update->keywords_seo=$request->keywords_seo;
            $update->description_seo=$request->description_seo;
            if($request->hasFile('image')){
                File::delete(public_path().'/img/'.$lastImg);
            }
            $update->save();
            $update->fileUpload($request);
            return redirect(route('admin_articles'));
        }
    }
}