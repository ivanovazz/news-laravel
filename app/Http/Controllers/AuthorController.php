<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 21.07.17
 * Time: 17:03
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Author;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class AuthorController extends Controller
{
    public function data(){
        //$admin_name=Auth::user()->login;
        $authors=\App\Author::all();
        return view('admin.author',['authors'=>$authors]);
    }
    public function add(Request $request){
        $author=new Author();
        $author->login=$request->login;
        $author->pass=Hash::make($request->pass);
        $author->save();
        return redirect(route('admin_authors'));
    }
    public function delete(Request $request,$id){
        if($request->route('id')){
            $delAuthor=\App\Author::find($id);
            $delAuthor->delete();
        }
        return redirect(route('admin_authors'));
    }
    public function beforeUpdate(Request $request,$id){
        if($request->route('id')){
            //$admin_name=Auth::user()->login;
            $beforeUpdate=\App\Author::find($id);
            return view('admin.updateAuthor',['beforeUpdate'=>$beforeUpdate]);
        }
    }
    public function update(Request $request,$id){
        if($request->route('id')){
            $update=\App\Author::find($id);
            $update->login=$request->login;
            $update->pass=Hash::make($request->pass);
            $update->save();
        }
        return redirect(route('admin_authors'));
    }
}