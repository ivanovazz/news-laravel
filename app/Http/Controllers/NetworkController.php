<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 02.08.17
 * Time: 16:30
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Network;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class NetworkController
{
    public function data(){
        $networks=Network::all();
        return view('admin.network', ['networks' => $networks]);
    }
    public function add(Request $request){
        $network=new Network();
        $network->network_name=$request->network_name;
        $network->route = $request->route;
        $network->save();
        return redirect(route('admin_networks'));
    }
    public function delete(Request $request,$id){
        if($id){
            $delNetwork=Network::findorFail($id);
            $delNetwork->delete();
        }
        return redirect(route('admin_networks'));
    }
    public function beforeUpdate(Request $request,$id){
        if($request->route('id')){
            //$admin_name=Auth::user()->login;
            $beforeUpdate=Network::find($id);
            return view('admin.updateNetwork',['beforeUpdate'=>$beforeUpdate]);
        }

    }
    public function update(Request $request, $id){
        if($request->route('id')){
            $update=Network::find($id);
            $update->network_name = $request->network_name;
            $update->route = $request->route;
            $update->save();
        }
        return redirect(route('admin_networks'));
    }
}