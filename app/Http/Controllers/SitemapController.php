<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 29.07.17
 * Time: 15:20
 */

namespace App\Http\Controllers;

use App\Article;
use App\View;
use App\Category;
use App\Author;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\MySqlConnection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Events\PostHasViewed;
use Closure;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;


class SitemapController extends Controller
{
    public function sitemap()
    {
        $categories=\App\Category::all();


        $articles = \App\Article::all();


        //$content = View::make('frontEnd.sitemap',['categories'=>$categories,'articles'=>$articles]);
        return Response::view('frontEnd.sitemap',['categories'=>$categories,'articles'=>$articles])->header('Content-Type', 'text/xml');

    }
}