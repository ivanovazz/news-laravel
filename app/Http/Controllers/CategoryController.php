<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 20.07.17
 * Time: 15:24
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Category;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class CategoryController extends Controller
{
    public function data()
    {

        //dd(Auth::user()->login);
        //$admin_name=Auth::user()->login;
        $categories = \App\Category::all();

        //$news = news::all();

        return view('admin.category', [
            'categories' => $categories,
            //'news' => $news
            //'admin_name'=>$admin_name
        ]);
    }
    public function addCategory(Request $request){
        //Category::create(Input::all());
        //$admin_name=Auth::user()->login;
        $category = new Category;
        $category->category_name = $request->category_name;
        $category->category_url = $request->category_url;
        $category->save();
        return redirect(route('admin_categories'));
        //return view('admin.category', ['admin_name'=>$admin_name]);

    }
    public function delete(Request $request, $id){
        if($id){
            $delCategory=\App\Category::find($id);
            $delCategory->delete();
        }
        //$category = new Category;
        //$category->id=$request->id;
        //$delCategory=\App\Category::find($category->id);
        //$delCategory->delete();
        return redirect(route('admin_categories'));

    }
    public function beforeUpdate(Request $request,$id){
        if($request->route('id')){
            //$admin_name=Auth::user()->login;
            $beforeUpdate=\App\Category::find($id);
            return view('admin.updateCategory',['beforeUpdate'=>$beforeUpdate]);
        }

    }
    public function update(Request $request, $id){
        if($request->route('id')){
            $update=\App\Category::find($id);
            $update->category_name = $request->category_name;
            $update->category_url = $request->category_url;
            $update->save();
        }
        return redirect(route('admin_categories'));
    }
    /*public function my2()
    {
        $categories = \App\Category::query()->first();

        return view('welcome', [
            'categories' => [$categories]
        ]);

    }*/


}
