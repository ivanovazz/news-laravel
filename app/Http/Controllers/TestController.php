<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class TestController extends Controller
{
    public function my()
    {
        $categories = \App\Category::all();

        //$news = news::all();

        return view('welcome', [
            'categories' => $categories,
            //'news' => $news
        ]);
    }

    public function my2()
    {
        $categories = \App\Category::query()->first();

        return view('welcome', [
            'categories' => [$categories]
        ]);
    }


}
