<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 24.07.17
 * Time: 16:35
 */

namespace App\Http\Controllers;

use App\Article;
use App\Network;
use App\View;
use App\Category;
use App\Author;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\MySqlConnection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Events\PostHasViewed;

class FrontEndController extends Controller
{
    public function header(){
        $categories = \App\Category::all();
        $networks = Network::all();
        $latestNews =Article::where('published','=',1)->orderBy('published_date_time','desc')->skip(0)->take(5)->get();
        return ['categories'=>$categories, 'latestNews'=>$latestNews, 'networks'=>$networks];
    }
    public function index(){
        $header=$this->header();
        $categories = \App\Category::all();
        $categoryArticles=[];

        foreach ($categories as $category){
            $articles=\App\Article::categoryArticles($category->id);
            $categoryArticles=array_add($categoryArticles,$category->id,$articles);

        }
        $popularArticles=Article::popularArticles();
        $latestNews =Article::where('published','=',1)->orderBy('published_date_time','desc')->skip(0)->take(5)->get();
        return view('frontEnd.index',['header'=>$header,'categories'=>$categories, 'latestNews'=>$latestNews,'categoryArticles'=>$categoryArticles,'popularArticles'=>$popularArticles]);
    }
    public function article(Request $request,$id){
        $header=$this->header();
        if(!$id){
            throw new Exception('no id');
        }
        if($id){
            $article=\App\Article::findOrFail($id);
            if(!$article){
                return redirect('/404');
            }
            $category=\App\Category::find($article->category_id);
            $author=\App\Author::find($article->author_id);
            $ip=$request->ip();
            $date=date('Y-m-d');
            //$viewBefore=\App\View::where('ip','=',$ip)->where('date','=',$date)->where('article_id','=',$id)->get();
            //if($viewBefore->isEmpty()){
                //event(new PostHasViewed($article));
            //}
            event(new PostHasViewed($article,$ip,$id,$date));
            \App\View::add($ip,$id,$date);
            $articleBefore = DB::table('articles')->where('published','=',1)->where('published_date_time','<',$article->published_date_time)->orderBy('published_date_time','desc')->skip(0)->take(1)->get();
            $articleAfter = DB::table('articles')->where('published','=',1)->where('published_date_time','>',$article->published_date_time)->orderBy('published_date_time','desc')->skip(0)->take(1)->get();
            $popularArticles=Article::popularArticles();
        }
        return view('frontEnd.article', ['header'=>$header,'article'=>$article, 'category'=>$category, 'author'=>$author, 'articleBefore'=>$articleBefore,'articleAfter'=>$articleAfter,'popularArticles'=>$popularArticles]);
    }
    public function category($id){
        $header=$this->header();
        if($id){
            $articles=Article::where('category_id','=',$id)->where('published','=',1)->orderBy('published_date_time','desc')->paginate(2);
            $category=\App\Category::find($id);
            $popularArticles=Article::popularArticles();
        }
        return view('frontEnd.category',['header'=>$header, 'category'=>$category, 'articles'=>$articles,'popularArticles'=>$popularArticles]);
    }
    public function search()
    {
        $header=$this->header();
        $q = Input::get('search');

        $posts = Article::whereRaw(
            "MATCH(title,small_text,text) AGAINST('*{$q}*' IN BOOLEAN MODE)",
            array($q)
        )->orderBy('published_date_time','desc')->paginate(2);
        $popularArticles=Article::popularArticles();
        return view('frontEnd.search',['header'=>$header,'posts'=>$posts,'popularArticles'=>$popularArticles,'q'=>$q]);

    }
    public function error(){
        $header=$this->header();
        $popularArticles=Article::popularArticles();
        return view('frontEnd.error',['header'=>$header,'popularArticles'=>$popularArticles]);
    }
}