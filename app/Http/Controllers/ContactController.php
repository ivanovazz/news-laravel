<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 02.08.17
 * Time: 15:55
 */

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Network;

class ContactController extends Controller
{
    public function header(){
        $categories = Category::all();
        $networks = Network::all();
        $latestNews =Article::where('published','=',1)->orderBy('published_date_time','desc')->skip(0)->take(5)->get();
        return ['categories'=>$categories, 'latestNews'=>$latestNews, 'networks'=>$networks];
    }
    public function create(){
        $header=$this->header();
        $popularArticles=Article::popularArticles();
        return view('frontEnd.contact',['header'=>$header,'popularArticles'=>$popularArticles]);
    }
    public function store(){}
}