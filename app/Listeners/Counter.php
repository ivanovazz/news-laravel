<?php

namespace App\Listeners;

use App\Events\PostHasViewed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \App\View;

class Counter
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostHasViewed  $event
     * @return void
     */
    public function handle(PostHasViewed $event)
    {
        $viewBefore=View::where('ip','=',$event->ip)->where('date','=',$event->date)->where('article_id','=',$event->article_id)->get();
        if($viewBefore->isEmpty()){
            $event->article->increment('view_count');
        }

    }
}
