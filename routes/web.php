<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    $categories = \App\Category::all();
//    return view('welcome', [
//        'categories' => $categories
//    ]);
//});

//Route::get('/', 'TestController@my');

use \Illuminate\Support\Facades\Route;
use \Illuminate\Support\Facades\Auth;

Route::get('/my-route', 'TestController@my2');
Route::group(['middleware' => 'auth'], function (){
    Route::get('/admin/', function () {
        //dd('asdsd');
        return redirect('/admin/category');
    });
    Route::get('/admin/category', 'CategoryController@data')->name('admin_categories');
    Route::get('/admin/category/addCategory',function (){
        //$admin_name=Auth::user()->login;
        return view('admin.addCategory');
    });
    Route::post('/admin/category/addCategory', 'CategoryController@addCategory');
    Route::get('/admin/category/deleteCategory/{id}', 'CategoryController@delete');
    Route::get('/admin/category/updateCategory/{id}', 'CategoryController@beforeUpdate');
    Route::post('/admin/category/updateCategory/{id}', 'CategoryController@update');


    Route::get('/admin/authors','AuthorController@data')->name('admin_authors');
    Route::get('/admin/authors/addAuthor', function (){
        //$admin_name=Auth::user()->login;
        return view('admin.addAuthor');
    });
    Route::post('/admin/authors/addAuthor', 'AuthorController@add');
    Route::get('admin/authors/deleteAuthor/{id}', 'AuthorController@delete');
    Route::get('admin/authors/updateAuthor/{id}', 'AuthorController@beforeUpdate');
    Route::post('admin/authors/updateAuthor/{id}', 'AuthorController@update');

    Route::get('/admin/articles', 'ArticleController@data')->name('admin_articles');
    Route::get('/admin/articles/addArticle', 'ArticleController@beforeAdd');
    Route::post('/admin/articles/addArticle', 'ArticleController@add');
    Route::get('/admin/articles/deleteArticle/{id}', 'ArticleController@delete');
    Route::get('/admin/articles/updateArticle/{id}', 'ArticleController@beforeUpdate');
    Route::post('/admin/articles/updateArticle/{id}', 'ArticleController@update');


    Route::get('/admin/logout', 'AdminLoginController@logout');

    Route::get('/admin/network', 'NetworkController@data')->name('admin_networks');
    Route::get('/admin/network/addNetwork', function(){
        return view('admin.addNetwork');
    });
    Route::post('/admin/network/addNetwork', 'NetworkController@add');
    Route::get('admin/network/deleteNetwork/{id}', 'NetworkController@delete');
    Route::get('/admin/network/updateNetwork/{id}', 'NetworkController@beforeUpdate');
    Route::post('/admin/network/updateNetwork/{id}', 'NetworkController@update');

});
Route::get('/admin/login', 'AdminLoginController@showLoginForm');
Route::post('/admin/login', ['as'=>'login', 'uses'=>'AdminLoginController@login']);
//Route::post('/admin/login', 'AdminLoginController@login');


Route::get('/', 'FrontEndController@index');
Route::get('/article/{id}-{slug_url}', 'FrontEndController@article');
Route::get('/category/{id}-{category_url}', 'FrontEndController@category');
Route::any('/search', 'FrontEndController@search');
Route::get('/sitemap', 'SitemapController@sitemap');
Route::get('/404', 'FrontEndController@error');
Route::get('/contact', ['as' => 'contact', 'uses' => 'ContactController@create']);
Route::post('/contact', ['as' => 'contact_store', 'uses' => 'ContactController@store']);
//Route::get("sitemap.xml", array(
   // "as"   => "sitemap",
   // "uses" => 'SitemapController@sitemap'
//));

/*Route::get('/', function () {
    return view('welcome');
});*/
