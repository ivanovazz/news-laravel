<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 25.07.17
 * Time: 18:26
 */
?>
<header id="header">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="header_top">
                <div class="header_top_left">
                    <ul class="top_nav">
                        <li><a href="/">Home</a></li>
                        <li><a href="/about">About</a></li>
                        <li><a href="/contact">Contact</a></li>
                    </ul>
                    <form action="/search" method="get">
                        <div class="lastAdd">
                            <input type="text" name="search">
                            <input class="btn btn-default" type="submit"  value="Search!">

                        </div>
                    </form>
                </div>
                <div class="header_top_right">
                    <p>Friday, December 05, 2045</p>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="header_bottom">
                <div class="logo_area"><a href="index.html" class="logo"><img src="/newsfeed/images/logo.jpg" alt=""></a></div>
                <div class="add_banner"><a href="#"><img src="/newsfeed/images/addbanner_728x90_V1.jpg" alt=""></a></div>
            </div>
        </div>
    </div>
</header>
<section id="navArea">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav main_nav">
                <li class="active"><a href="/"><span class="fa fa-home desktop-home"></span><span class="mobile-show">Home</span></a></li>
                @foreach($header['categories'] as $category)
                    <li><a href="/category/{{$category->id}}-{{$category->category_url}}">{{$category->category_name}}</a></li>
                @endforeach
            </ul>
        </div>
    </nav>
</section>
<section id="newsSection">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="latest_newsarea"> <span>Latest News</span>
                <ul id="ticker01" class="news_sticker">
                    @foreach($header['latestNews'] as $val)
                        <li><a href="/article/{{$val->id}}-{{$val->slug_url}}"><img src="/img/{{$val->img}}" alt="">{{$val->title}}</a></li>
                    @endforeach
                </ul>
                <div class="social_area">
                    <ul class="social_nav">
                        @foreach($header['networks'] as $val)
                            <li class="{{$val->network_name}}"><a href="{{$val->route}}"></a></li>
                        @endforeach
                        <!--<li class="facebook"><a href="#"></a></li>
                        <li class="twitter"><a href="#"></a></li>
                        <li class="flickr"><a href="#"></a></li>
                        <li class="pinterest"><a href="#"></a></li>
                        <li class="googleplus"><a href="#"></a></li>
                        <li class="vimeo"><a href="#"></a></li>
                        <li class="youtube"><a href="#"></a></li>
                        <li class="mail"><a href="#"></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
