<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 24.07.17
 * Time: 16:45
 */
?>
        <!DOCTYPE html>
<html>
<head>
    <title>NewsFeed</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/font.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/li-scroller.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/theme.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/style.css">
    <!--[if lt IE 9]>
    <script src="/newsfeed/assets/js/html5shiv.min.js"></script>
    <script src="/newsfeed/assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
    @include('frontEnd.layouts.header')
    <section id="sliderSection">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="slick_slider">
                    @foreach($latestNews as $val)
                    <div class="single_iteam"> <a href="/article/{{$val->id}}-{{$val->slug_url}}"> <img src="/img/{{$val->img}}" alt=""></a>
                        <div class="slider_article">
                            <h2><a class="slider_tittle" href="/article/{{$val->id}}-{{$val->slug_url}}">{{$val->title}}</a></h2>
                            <p>{{$val->small_text}}</p>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="latest_post">
                    <h2><span>Latest post</span></h2>
                    <div class="latest_post_container">
                        <div id="prev-button"><i class="fa fa-chevron-up"></i></div>
                        <ul class="latest_postnav">
                            @foreach($latestNews as $val)
                            <li>
                                <div class="media"> <a href="/article/{{$val->id}}-{{$val->slug_url}}" class="media-left"> <img alt="" src="/img/{{$val->img}}"> </a>
                                    <div class="media-body"> <a href="/article/{{$val->id}}-{{$val->slug_url}}" class="catg_title"> {{$val->title}}</a> </div>
                                </div>
                            </li>
                            @endforeach

                        </ul>
                        <div id="next-button"><i class="fa  fa-chevron-down"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contentSection">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="left_content">
                    @foreach($categories as $category)
                    <div class="single_post_content">
                        <h2><span>{{$category->category_name}}</span></h2>
                        @foreach($categoryArticles[$category->id] as $k=>$v)
                            @if($k==0)
                        <div class="single_post_content_left">
                            <ul class="business_catgnav  wow fadeInDown">
                                <li>
                                    <figure class="bsbig_fig"> <a href="/article/{{$v->id}}-{{$v->slug_url}}" class="featured_img"> <img alt="" src="/img/{{$v->img}}"> <span class="overlay"></span> </a>
                                        <figcaption> <a href="/article/{{$v->id}}-{{$v->slug_url}}">{{$v->title}}</a> </figcaption>
                                        <p>{{$v->small_text}}</p>
                                    </figure>
                                </li>
                            </ul>
                        </div>
                            @else
                        <div class="single_post_content_right">
                            <ul class="spost_nav">
                                <li>
                                    <div class="media wow fadeInDown"> <a href="/article/{{$v->id}}-{{$v->slug_url}}" class="media-left"> <img alt="" src="/img/{{$v->img}}"> </a>
                                        <div class="media-body"> <a href="/article/{{$v->id}}-{{$v->slug_url}}" class="catg_title"> {{$v->title}}</a> </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                            @endif
                        @endforeach
                    </div>

                    @endforeach

                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <aside class="right_content">
                    <div class="single_sidebar">
                        <h2><span>Popular Post</span></h2>
                        <ul class="spost_nav">
                            @foreach($popularArticles as $val)
                            <li>
                                <div class="media wow fadeInDown"> <a href="/article/{{$val->id}}-{{$val->slug_url}}" class="media-left"> <img alt="" src="/img/{{$val->img}}"> </a>
                                    <div class="media-body"> <a href="/article/{{$val->id}}-{{$val->slug_url}}" class="catg_title"> {{$val->title}}</a> </div>
                                </div>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                    <div class="single_sidebar">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#category" aria-controls="home" role="tab" data-toggle="tab">Category</a></li>
                            <li role="presentation"><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Video</a></li>
                            <li role="presentation"><a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Comments</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="category">
                                <ul>
                                    <li class="cat-item"><a href="#">Sports</a></li>
                                    <li class="cat-item"><a href="#">Fashion</a></li>
                                    <li class="cat-item"><a href="#">Business</a></li>
                                    <li class="cat-item"><a href="#">Technology</a></li>
                                    <li class="cat-item"><a href="#">Games</a></li>
                                    <li class="cat-item"><a href="#">Life &amp; Style</a></li>
                                    <li class="cat-item"><a href="#">Photography</a></li>
                                </ul>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="video">
                                <div class="vide_area">
                                    <iframe width="100%" height="250" src="http://www.youtube.com/embed/h5QWbURNEpA?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="comments">
                                <ul class="spost_nav">
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="/newsfeed/images/post_img1.jpg"> </a>
                                            <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 1</a> </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="/newsfeed/images/post_img2.jpg"> </a>
                                            <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 2</a> </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="/newsfeed/images/post_img1.jpg"> </a>
                                            <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 3</a> </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="pages/single_page.html" class="media-left"> <img alt="" src="/newsfeed/images/post_img2.jpg"> </a>
                                            <div class="media-body"> <a href="pages/single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 4</a> </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="single_sidebar wow fadeInDown">
                        <h2><span>Sponsor</span></h2>
                        <a class="sideAdd" href="#"><img src="/newsfeed/images/add_img.jpg" alt=""></a> </div>
                    <div class="single_sidebar wow fadeInDown">
                        <h2><span>Category Archive</span></h2>
                        <select class="catgArchive">
                            <option>Select Category</option>
                            <option>Life styles</option>
                            <option>Sports</option>
                            <option>Technology</option>
                            <option>Treads</option>
                        </select>
                    </div>
                    <div class="single_sidebar wow fadeInDown">
                        <h2><span>Links</span></h2>
                        <ul>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Rss Feed</a></li>
                            <li><a href="#">Login</a></li>
                            <li><a href="#">Life &amp; Style</a></li>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </section>
    @include('frontEnd.layouts.footer')
</div>
<script src="/newsfeed/assets/js/jquery.min.js"></script>
<script src="/newsfeed/assets/js/wow.min.js"></script>
<script src="/newsfeed/assets/js/bootstrap.min.js"></script>
<script src="/newsfeed/assets/js/slick.min.js"></script>
<script src="/newsfeed/assets/js/jquery.li-scroller.1.0.js"></script>
<script src="/newsfeed/assets/js/jquery.newsTicker.min.js"></script>
<script src="/newsfeed/assets/js/jquery.fancybox.pack.js"></script>
<script src="/newsfeed/assets/js/custom.js"></script>
</body>
</html>
