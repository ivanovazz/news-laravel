<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 31.07.17
 * Time: 14:52
 */
//header('Content-Type', 'application/xml');
$xml = new \SimpleXMLElement('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');
echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($categories as $category)
        <url>
            <loc>http://news-laravel/category/{{$category->id}}-{{$category->category_name}}</loc>
            <lastmod>{{$category->updated_at}}</lastmod>
            <changefreq>daily</changefreq>
            <priority>1.0</priority>
        </url>
    @endforeach

    @foreach($articles as $article)
        <url>
            <loc>http://news-laravel/article/{{$article->id}}-{{$article->slug_url}}</loc>
            <lastmod>{{$article->published_date_time}}</lastmod>
            <changefreq>daily</changefreq>
            <priority>1.0</priority>
        </url>
    @endforeach
</urlset>
