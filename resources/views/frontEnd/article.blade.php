<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 25.07.17
 * Time: 18:20
 */
?>
        <!DOCTYPE html>
<html>
<head>
    <title>NewsFeed | Pages | Single Page</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/font.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/li-scroller.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/theme.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/style.css">
    <!--[if lt IE 9]>
    <script src="/newsfeed/assets/js/html5shiv.min.js"></script>
    <script src="/newsfeed/assets/js/respond.min.js"></script>

    <![endif]-->
</head>
<body>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
    @include('frontEnd.layouts.header')
    <section id="contentSection">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="left_content">
                    <div class="single_page">
                        <ol class="breadcrumb">
                            <li><a href="/">Home</a></li>
                            <li class="active"><a href="#">{{$category->category_name}}</a></li>

                        </ol>
                        <h1>{{$article->title}}</h1>
                        <div class="post_commentbox"> <i class="fa fa-user"></i> {{$author->login}} <span><i class="fa fa-calendar"></i>{{$article->published_date_time}}</span> <a href="#"><i class="fa fa-tags"></i>{{$category->category_name}}</a> </div>
                        <div class="single_page_content"> <img class="img-center" src="/img/{{$article->img}}" alt="">
                            <p>{{$article->text}}</p>
                            <h2>This is h2 title</h2>
                            <h3>This is h3 title</h3>
                            <h4>This is h4 title</h4>
                            <h5>This is h5 title</h5>
                            <h6>This is h6 Title</h6>
                            <button class="btn default-btn">Default</button>
                            <button class="btn btn-red">Red Button</button>
                            <button class="btn btn-yellow">Yellow Button</button>
                            <button class="btn btn-green">Green Button</button>
                            <button class="btn btn-black">Black Button</button>
                            <button class="btn btn-orange">Orange Button</button>
                            <button class="btn btn-blue">Blue Button</button>
                            <button class="btn btn-lime">Lime Button</button>
                            <button class="btn btn-theme">Theme Button</button>
                        </div>
                        <div class="social_link">
                            <ul class="sociallink_nav">
                                <li><a onclick="Share.facebook('URL','TITLE','IMG_PATH','DESC')"><i class="fa fa-facebook"></i></a></li>
                                <li><a onclick="Share.twitter('URL','TITLE')"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                        <div class="related_post">
                            <h2>Related Post <i class="fa fa-thumbs-o-up"></i></h2>
                            <ul class="spost_nav wow fadeInDown animated">
                                <li>
                                    <div class="media"> <a class="media-left" href="single_page.html"> <img src="../images/post_img1.jpg" alt=""> </a>
                                        <div class="media-body"> <a class="catg_title" href="single_page.html"> Aliquam malesuada diam eget turpis varius</a> </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media"> <a class="media-left" href="single_page.html"> <img src="../images/post_img2.jpg" alt=""> </a>
                                        <div class="media-body"> <a class="catg_title" href="single_page.html"> Aliquam malesuada diam eget turpis varius</a> </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media"> <a class="media-left" href="single_page.html"> <img src="../images/post_img1.jpg" alt=""> </a>
                                        <div class="media-body"> <a class="catg_title" href="single_page.html"> Aliquam malesuada diam eget turpis varius</a> </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="nav-slit"> <a @if($articleBefore->isEmpty()) style="display: none" @endif class="prev" href="/article/@if(!$articleBefore->isEmpty()){{$articleBefore[0]->id}}-{{$articleBefore[0]->slug_url}}@endif"> <span class="icon-wrap"><i class="fa fa-angle-left"></i></span>
                    <div>
                        <h3>@if(!$articleBefore->isEmpty()){{$articleBefore[0]->title}}@endif</h3>
                        <img src="/img/@if(!$articleBefore->isEmpty()){{$articleBefore[0]->img}}@endif" alt=""/> </div>
                </a > <a @if($articleAfter->isEmpty()) style="display: none" @endif class="next" href="/article/@if(!$articleAfter->isEmpty()){{$articleAfter[0]->id}}-{{$articleAfter[0]->slug_url}}@endif"> <span class="icon-wrap"><i class="fa fa-angle-right"></i></span>
                    <div>
                        <h3>@if(!$articleAfter->isEmpty()){{$articleAfter[0]->title}}@endif</h3>
                        <img src="/img/@if(!$articleAfter->isEmpty()){{$articleAfter[0]->img}}@endif" alt=""/> </div>
                </a >  </nav>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <aside class="right_content">
                    <div class="single_sidebar">
                        <h2><span>Popular Post</span></h2>
                        <ul class="spost_nav">
                            @foreach($popularArticles as $val)
                                <li>
                                    <div class="media wow fadeInDown"> <a href="/article/{{$val->id}}-{{$val->slug_url}}" class="media-left"> <img alt="" src="/img/{{$val->img}}"> </a>
                                        <div class="media-body"> <a href="/article/{{$val->id}}-{{$val->slug_url}}" class="catg_title"> {{$val->title}}</a> </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="single_sidebar">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#category" aria-controls="home" role="tab" data-toggle="tab">Category</a></li>
                            <li role="presentation"><a href="#video" aria-controls="profile" role="tab" data-toggle="tab">Video</a></li>
                            <li role="presentation"><a href="#comments" aria-controls="messages" role="tab" data-toggle="tab">Comments</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="category">
                                <ul>
                                    <li class="cat-item"><a href="#">Sports</a></li>
                                    <li class="cat-item"><a href="#">Fashion</a></li>
                                    <li class="cat-item"><a href="#">Business</a></li>
                                    <li class="cat-item"><a href="#">Technology</a></li>
                                    <li class="cat-item"><a href="#">Games</a></li>
                                    <li class="cat-item"><a href="#">Life &amp; Style</a></li>
                                    <li class="cat-item"><a href="#">Photography</a></li>
                                </ul>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="video">
                                <div class="vide_area">
                                    <iframe width="100%" height="250" src="http://www.youtube.com/embed/h5QWbURNEpA?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="comments">
                                <ul class="spost_nav">
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="single_page.html" class="media-left"> <img alt="" src="../images/post_img1.jpg"> </a>
                                            <div class="media-body"> <a href="single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 1</a> </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="single_page.html" class="media-left"> <img alt="" src="../images/post_img2.jpg"> </a>
                                            <div class="media-body"> <a href="single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 2</a> </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="single_page.html" class="media-left"> <img alt="" src="../images/post_img1.jpg"> </a>
                                            <div class="media-body"> <a href="single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 3</a> </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="single_page.html" class="media-left"> <img alt="" src="../images/post_img2.jpg"> </a>
                                            <div class="media-body"> <a href="single_page.html" class="catg_title"> Aliquam malesuada diam eget turpis varius 4</a> </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="single_sidebar wow fadeInDown">
                        <h2><span>Sponsor</span></h2>
                        <a class="sideAdd" href="#"><img src="../images/add_img.jpg" alt=""></a> </div>
                    <div class="single_sidebar wow fadeInDown">
                        <h2><span>Category Archive</span></h2>
                        <select class="catgArchive">
                            <option>Select Category</option>
                            <option>Life styles</option>
                            <option>Sports</option>
                            <option>Technology</option>
                            <option>Treads</option>
                        </select>
                    </div>
                    <div class="single_sidebar wow fadeInDown">
                        <h2><span>Links</span></h2>
                        <ul>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Rss Feed</a></li>
                            <li><a href="#">Login</a></li>
                            <li><a href="#">Life &amp; Style</a></li>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </section>
    @include('frontEnd.layouts.footer')
</div>
<script>
    Share={
        facebook: function(purl, ptitle, pimg, text) {
            url  = 'http://www.facebook.com/sharer.php?s=100';
            url += '&p[title]='     + encodeURIComponent(ptitle);
            url += '&p[summary]='   + encodeURIComponent(text);
            url += '&p[url]='       + encodeURIComponent(purl);
            url += '&p[images][0]=' + encodeURIComponent(pimg);
            Share.popup(url);
        },
        twitter: function(purl, ptitle) {
            url  = 'http://twitter.com/share?';
            url += 'text='      + encodeURIComponent(ptitle);
            url += '&url='      + encodeURIComponent(purl);
            url += '&counturl=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        popup: function(url) {
            window.open(url,'','toolbar=0,status=0,width=626,height=436');
        }
    }
</script>
<script src="/newsfeed/assets/js/jquery.min.js"></script>
<script src="/newsfeed/assets/js/wow.min.js"></script>
<script src="/newsfeed/assets/js/bootstrap.min.js"></script>
<script src="/newsfeed/assets/js/slick.min.js"></script>
<script src="/newsfeed/assets/js/jquery.li-scroller.1.0.js"></script>
<script src="/newsfeed/assets/js/jquery.newsTicker.min.js"></script>
<script src="/newsfeed/assets/js/jquery.fancybox.pack.js"></script>
<script src="/newsfeed/assets/js/custom.js"></script>
</body>
</html>
