<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 01.08.17
 * Time: 16:15
 */
?>
        <!DOCTYPE html>
<html>
<head>
    <title>NewsFeed | Pages | 404</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/font.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/li-scroller.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/theme.css">
    <link rel="stylesheet" type="text/css" href="/newsfeed/assets/css/style.css">
    <!--[if lt IE 9]>
    <script src="../assets/js/html5shiv.min.js"></script>
    <script src="../assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
    @include('frontEnd.layouts.header')
    <section id="contentSection">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="left_content">
                    <div class="error_page">
                        <h3>We Are Sorry</h3>
                        <h1>404</h1>
                        <p>Unfortunately, the page you were looking for could not be found. It may be temporarily unavailable, moved or no longer exists</p>
                        <span></span> <a href="/" class="wow fadeInLeftBig">Go to home page</a> </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <aside class="right_content">
                    <div class="single_sidebar">
                        <h2><span>Popular Post</span></h2>
                        <ul class="spost_nav">
                            <ul class="spost_nav">
                                @foreach($popularArticles as $val)
                                    <li>
                                        <div class="media wow fadeInDown"> <a href="/article/{{$val->id}}-{{$val->slug_url}}" class="media-left"> <img alt="" src="/img/{{$val->img}}"> </a>
                                            <div class="media-body"> <a href="/article/{{$val->id}}-{{$val->slug_url}}" class="catg_title"> {{$val->title}}</a> </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </section>
    @include('frontEnd.layouts.footer')
</div>
<script src="/newsfeed/assets/js/jquery.min.js"></script>
<script src="/newsfeed/assets/js/wow.min.js"></script>
<script src="/newsfeed/assets/js/bootstrap.min.js"></script>
<script src="/newsfeed/assets/js/slick.min.js"></script>
<script src="/newsfeed/assets/js/jquery.li-scroller.1.0.js"></script>
<script src="/newsfeed/assets/js/jquery.newsTicker.min.js"></script>
<script src="/newsfeed/assets/js/jquery.fancybox.pack.js"></script>
<script src="/newsfeed/assets/js/custom.js"></script>
</body>
</html>
